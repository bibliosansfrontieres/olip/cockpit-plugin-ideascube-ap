export default class Hostapd {

    constructor() {
        systemd_client = cockpit.dbus("systemd");
        systemd_manager = systemd_client.proxy("org.freedesktop.systemd1.Manager","/org/freedesktop/systemd1");
    }

    watchConf() {
        cockpit.file('/etc/hostapd/hostapd.conf').watch((content) => {
            if (content) {
                this.setState({ hostapdConfFileContent: content });

                let contentTable = content.split("\n");
                for (var i in contentTable) {
                    if (contentTable[i].split("=")[0] == "ssid") {
                        this.setState({ ssid: contentTable[i].split("=")[1] });
                    } else if (contentTable[i].split("=")[0] == "wpa_passphrase") {
                        this.setState({ wpa_passphrase: contentTable[i].split("=")[1] });
                    } else if (contentTable[i].split("=")[0] == "wpa_key_mgmt") {
                        this.setState({ wpa_key_mgmt: contentTable[i].split("=")[1] });
                    }
                }
            }
        });
    }

    getStatus(){
        var proc = cockpit.spawn(["systemctl", "status", "hostapd"]);
            proc.stream((data) => {
                if (data.indexOf("running") != -1) {
                    this.setState({ hostapdStatus: "UP" });
                    return "UP";
                } else {
                    this.setState({ hostapdStatus: "DOWN" });
                    return "DOWN";
                }
            });
    }

    start(){
        var proc = cockpit.spawn(["systemctl", "start", "hostapd"]);
        return proc;
    }
    
    stop(){
        var proc = cockpit.spawn(["systemctl", "stop", "hostapd"]);
        return proc;
    }

    changePassword(newPassword){
        cockpit.file('/etc/hostapd/hostapd.conf').modify((old_content) => {
            if (old_content) {
                let contentTable = old_content.split("\n");
                for (var i in contentTable) {
                    if (contentTable[i].split("=")[0] == "wpa_passphrase") {
                        contentTable[i] = "wpa_passphrase=" + newPassword
                    }
                }
                return contentTable.join("\n");
            }
        })
    }

    changeSSID(newSSID){
        cockpit.file('/etc/hostapd/hostapd.conf').modify((old_content) => {
            if (old_content) {
                let contentTable = old_content.split("\n");
                for (var i in contentTable) {
                    if (contentTable[i].split("=")[0] == "ssid") {
                        contentTable[i] = "ssid=" + newSSID
                    }
                }
                return contentTable.join("\n");
            }
        })
    }
}